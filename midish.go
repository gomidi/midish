package midish

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
)

var DEBUG = false

func New() (*MidiSh, error) {
	var ms MidiSh
	err := ms.findBinary()

	if err != nil {
		return nil, err
	}
	return &ms, nil
}

type MidiSh struct {
	binPath string
	//version    string
	isOpen     bool
	proc       *os.Process
	procwriter io.WriteCloser
	outReader  io.ReadCloser
	errReader  io.ReadCloser
	trackno    int
}

var stop = make(chan bool)

func (m *MidiSh) Start() (err error) {
	m.isOpen = true
	cmd := exec.Command(m.binPath, "-b", "-v")
	m.procwriter, _ = cmd.StdinPipe()
	m.errReader, _ = cmd.StderrPipe()
	m.outReader, _ = cmd.StdoutPipe()

	// TODO linux only
	//cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true, Pgid: 0}

	err = cmd.Start()
	m.proc = cmd.Process
	if DEBUG {
		fmt.Printf("PID: %v\n", m.proc.Pid)
	}
	return
}

func (m *MidiSh) WaitForErrMsg(waitfor string) error {
	if DEBUG {
		fmt.Printf("looking for %q in STDERR\n", waitfor)
	}
	for {
		var b = make([]byte, 1024)
		i, err := m.errReader.Read(b)
		if i > 0 {
			res := string(b[:i])
			if DEBUG {
				fmt.Print("ERROR: \n####\n" + res + "\n####\n")
			}
			if strings.Contains(res, waitfor) {
				if DEBUG {
					fmt.Printf("found %q in STDERR\n", waitfor)
				}
				return nil
			}
		}
		if err != nil {
			return err
		}

	}
}

/*
while (!eof) {
        command = get_command_from_user();
        wait_for("+ready");
        write_to_midish_stdin(command);
        result = parse_midish_stdout();
        do_something(result);
}
*/

func (m *MidiSh) Close() error {
	if DEBUG {
		fmt.Println("closing")
	}
	m.Send("exit", "+ready")
	m.proc.Wait()
	_ = m.proc.Kill()
	m.proc.Release()
	return nil
}

func (m *MidiSh) WaitForMsg(waitfor string) error {
	if DEBUG {
		fmt.Printf("looking for %q in STDOUT\n", waitfor)
	}
	for {
		var b = make([]byte, 1024)
		i, err := m.outReader.Read(b)
		if i > 0 {
			res := string(b[:i])
			if DEBUG {
				fmt.Print("got: \n####\n" + res + "\n####\n")
			}
			if strings.Contains(res, waitfor) {
				if DEBUG {
					fmt.Printf("found %q in STDOUT\n", waitfor)
				}
				return nil
			}
		}
		if err != nil {
			return err
		}

	}
}

func (m *MidiSh) Send(cmd string, waitfor string) error {
	//if DEBUG {
	//}
	fmt.Printf("sending:\n----\n%s\n----\n", cmd)
	//m.mu.Lock()
	//defer m.mu.Unlock()
	if !m.isOpen {
		return fmt.Errorf("midish is not started")
	}

	cmds := strings.Split(cmd, "\n")
	for _, c := range cmds {
		_, err := m.procwriter.Write([]byte(c + "\n"))
		if err != nil {
			return fmt.Errorf("could not write to proc: %s", err.Error())
		}
		if waitfor != "" {
			err := m.WaitForMsg(waitfor)
			if err != nil {
				return fmt.Errorf("could not get read state: %s", err.Error())
			}
		}

	}

	return nil
}

/*
21 Using midish in other programs
21.1 Creating scripts: batch mode

Midish could be used from general purpose scripting languages to do MIDI-related tasks.
This is accomplished by starting the ``midish'' binary and writing commands to it's standard input.
To ease this process, midish should be started in batch mode, with the -b flag.
In batch mode the ``~/.midishrc'' and ``/etc/midishrc'' files are not parsed, errors cause midish to exit, and ``p'', ``r'' and ``i'' commands are blocking.

For instance the following simple shell script will play, on the ``/dev/rmidi1'' device, standard midi files enumerated on the command line:

#!/bin/sh

trap : 2

for arg; do
midish -b <<END
dnew 0 "/dev/rmidi1" wo
import "$arg"
p
END
done

The ``smfplay'' and ``smfrec'' files shipped in the source tar-balls are examples of such scripts.
21.2 Creating front-ends: verbose mode

A program that wants to use a midish feature, may start midish and issue commands on its standard input.
Then, the standard output of midish could be parsed so the program can obtain the desired information (if any).

To ease this process, the midish binary can be started with the -v flag; in this case it will write additional
information on its standard output, allowing the caller to be notified of changes of the state of midish.
The information is written on a single line starting with the + sign, as follows:

    +ready
    means that midish is ready to parse and execute a new line. A front-end should always wait for this string before issuing any command.
    +pos measure beat tick
    is written during performace mode on every beat, it gives the current song position.

No midish function (like print) can generate a line starting with the + sign, so it is safe to assume that such lines are synchronization lines and not the output of a function. Furthermore, such lines will never appear in the middle of the output of a function. Additional information may be available in the same format in future versions of midish; thus front-ends should be able to ignore unknown lines starting with +.

Generally, any front-end should use a loop similar to the following:

while (!eof) {
        command = get_command_from_user();
        wait_for("+ready");
        write_to_midish_stdin(command);
        result = parse_midish_stdout();
        do_something(result);
}

The ``rmidish'' program shipped in the source tar-ball is and example of such front-end.
22 Examples
22.1 Example - MIDI filtering

The following session show how to configure a keyboard split:

send EOF character (control-D) to quit
[0000:00]> inew kbd {1 0}
[0000:00]> onew bass {0 5}
[0000:00]> oaddev {pc bass 33}
[0000:00]> onew piano {0 6}
[0000:00]> oaddev {pc piano 2}
[0000:00]> fnew split
[0000:00]> fmap {any kbd} {any bass}
[0000:00]> fmap {any kbd} {any piano}
[0000:00]> fmap {note kbd 12..62} {note bass 0..50}
[0000:00]> fmap {note kbd 63..127} {note piano 63..127}
[0000:00]> finfo
{
        evmap any {1 0} > any {0 5}
        evmap any {1 0} > any {0 6}
        evmap note {1 0} 12..62 > note {0 5} 0..50
        evmap note {1 0} 63..127 > note {0 6} 63..127
}
[0000:00]> i
[0000:00]> s
[0000:00]> save "piano-bass"

First we set the default input to device 1, channel 6, on which the keyboard is available. Then we define 2 named-channels ``bass'' on device 0, channel 5 and ``piano'' on device 0 channel 6. Then we assign patches to the respective channels. After this, we define a new filter ``split'' and we add rules corresponding to the keyboard-split on note number 62 (note D3), the bass is transposed by -12 half-tones (one octave).
22.2 Example - recording a track

The following session show how to record a track.

send EOF character (control-D) to quit
[0000:00]> inew kbd {1 0}                    # select default input
[0000:00]> onew drums {0 9}                  # create drum output
[0000:00]> tnew dr1                          # create track ``dr1''
[0000:00]> t 90                              # tempo to 90 bpm
[0000:00]> r                                 # start recording
[0003:03]> s                                 # stop
[0000:00]> setq 16                           # set quantization step
[0000:00]> sel 32                            # select 32 measures
[0000:00]> tquanta 75                        # quantize to 75%
[0000:00]> p                                 # play
[0001:02]> s                                 # stop playing
[0000:00]> save "myrythm"                    # save to a file
[0000:00]>                                   # hit ^D to quit

first, we set the default input to ``{1 0}'' (the keyboard). Then, we define the ``drum'' output as device 0, channel 9, this creates a default filter that maps ``kbd'' to ``drums''. Then we define a new track named ``dr1'' an we start recording. Then, we set the quantization step to 16 (sixteenth note), we select the first 32 measures of the track and we quantize them. Finally, we start playback and we save the song into a file.
*/
