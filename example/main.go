package main

import (
	"fmt"
	"os"
	"time"

	"gitlab.com/gomidi/midish"
)

func main() {
	err := run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %s\n", err.Error())
		os.Exit(1)
	}
	os.Exit(0)
}

func run() (err error) {
	midish.DEBUG = false

	ms, err := midish.New()

	if err != nil {
		panic("can't find midish binary")
	}

	//fmt.Println("starting")
	err = ms.Start()
	if err != nil {
		return err
	}

	if len(os.Args) < 4 {
		fmt.Printf("usage: inport outport filetosave")
		os.Exit(1)
	}

	inPort := os.Args[1]
	outPort := os.Args[2]
	filename := os.Args[3] // midish-rec

	in := midish.AlsaPort(inPort)   // "VMPK Output:VMPK Output 131:0"
	out := midish.AlsaPort(outPort) // "FLUID Synth (qsynth):Synth input port (qsynth:0) 133:0"

	//fmt.Println("setting up midiports")
	err = ms.SetupMIDIPorts(in, out, 0, 0)
	if err != nil {
		return err
	}

	//fmt.Println("idling")
	err = ms.Idle()
	if err != nil {
		return err
	}

	time.Sleep(2 * time.Second)

	err = ms.Stop()
	if err != nil {
		return err
	}

	/*
		err = ms.SetupMetronomeKeys(0, 60, 40, 120, 100)
		if err != nil {
			return err
		}


	*/
	err = ms.SetupMetronome(true)
	if err != nil {
		return err
	}

	//fmt.Println("start recording")
	err = ms.Record(80)
	if err != nil {
		return err
	}

	time.Sleep(10 * time.Second)

	//fmt.Println("stopping")
	err = ms.Stop()
	if err != nil {
		return err
	}

	err = ms.Send("g 0", "+ready")
	if err != nil {
		return err
	}

	//fmt.Println("start recording")
	err = ms.Record(80)
	if err != nil {
		return err
	}

	time.Sleep(10 * time.Second)

	//fmt.Println("stopping")
	err = ms.Stop()
	if err != nil {
		return err
	}

	err = ms.Send("g 0", "+ready")
	if err != nil {
		return err
	}

	time.Sleep(2 * time.Second)

	err = ms.Play()
	if err != nil {
		return err
	}

	ms.WaitForErrMsg("playback stopped")

	/*
		fmt.Println("stopping")
		err = MidiSh.Stop()
		if err != nil {
			return err
		}

		time.Sleep(10 * time.Second)
		stop <- true
	*/

	err = ms.Save(filename)
	if err != nil {
		return err
	}
	err = ms.Export(filename + ".mid")
	if err != nil {
		return err
	}

	//fmt.Println("closing")
	return ms.Close()
}
