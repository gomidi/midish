package midish

import "regexp"

var alsamatch = regexp.MustCompile("([0-9]+" + regexp.QuoteMeta(":") + "[0-9]+)")

func AlsaPort(s string) string {
	// [10] VMPK Output:VMPK Output 131:0
	sub := alsamatch.FindString(s)
	if len(sub) == 0 {
		panic("can't find alsa port for " + s)
	}
	return sub
}
