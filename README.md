
# midish

Control midish (https://midish.org) from Go

You may send any command to midish, but there are only shortcuts for idling, playing, recording, 
loading, saving, and import/export to standard midi files.
