package midish

import (
	"fmt"
	"strings"
	"syscall"
)

func (m *MidiSh) SetupMIDIPorts(inPort, outPort string, channelIn uint8, channelOut uint8) error {
	return m.Send(strings.TrimSpace(fmt.Sprintf(`
dnew 0 %q wo
dnew 1 %q ro
fnew playing
fmap {any {1 %v}} {any {0 %v}}
`, outPort, inPort, int(channelIn), int(channelOut))), "+ready")
	// i
}

func (m *MidiSh) SetupMetronome(on bool) error {
	if on {
		return m.Send("m on", "+complete")
	}
	return m.Send("m off", "+complete")
}

func (m *MidiSh) SetupMetronomeKeys(channel, upkey, downkey, upvel, downvel uint8) error {
	return m.Send(strings.TrimSpace(fmt.Sprintf(`
metrocf {non {0 %v} %v %v} {non {0 %v} %v %v}
`, int(channel), int(upkey), int(upvel), int(channel), int(downkey), int(downvel))), "")
}

func (m *MidiSh) Idle() error {
	err := m.Send("i", "")
	m.WaitForErrMsg("press ^C to stop idling")
	return err
}

func (m *MidiSh) Record(tempobpm int) error {
	//return m.Send(fmt.Sprintf("t %v\ntnew %q\nr", tempobpm, track), "+ready")
	//err = m.Send(fmt.Sprintf("tnew %s", track), "+ready")
	m.trackno += 1
	err := m.Send(fmt.Sprintf("tnew rec%v", m.trackno), "")
	if err != nil {
		return err
	}
	//return m.Send("r", "+ready")
	err = m.Send("r", "")
	m.WaitForErrMsg("press ^C to stop recording")
	return err
}

func (m *MidiSh) Play() error {
	err := m.Send("p", "")
	m.WaitForErrMsg("press ^C to stop playback")
	return err
}

func (m *MidiSh) Stop() error {
	//return nil
	//return m.Send("s", "+complete")
	if DEBUG {
		fmt.Printf("sending SIGINT to PID %v\n", m.proc.Pid)
	}
	err := m.proc.Signal(syscall.SIGINT)
	//time.Sleep(time.Second)
	m.WaitForErrMsg("stopped")
	return err
}

func (m *MidiSh) Save(filename string) error {
	return m.Send(fmt.Sprintf("save %q", filename), "+complete")
}

func (m *MidiSh) Load(filename string) error {
	return m.Send(fmt.Sprintf("load %q", filename), "+ready")
}

func (m *MidiSh) Import(midifile string) error {
	return m.Send(fmt.Sprintf("import %q", midifile), "+ready")
}

func (m *MidiSh) Export(midifile string) error {
	return m.Send(fmt.Sprintf("export %q", midifile), "+ready")
}
