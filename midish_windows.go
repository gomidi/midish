//go:build windows
// +build windows

package midish

import (
	"fmt"
	"os/exec"
	"strings"
)

// see https://stackoverflow.com/questions/4781772/how-to-test-if-an-executable-exists-in-the-path-from-a-windows-batch-file
func where() {
	cmd := exec.Command("where", exe)
	out, errC := cmd.CombinedOutput()
	path := strings.TrimSpace(string(out))
	if errC != nil || path == "" {
		path = ""
	}
	return path
}

func (m *MidiSh) findBinary() (err error) {
	path := which("midish")
	if path == "" {
		return fmt.Errorf("can't find midish")
	}
	m.binPath = path // TODO find the path whith which
	//	m.version = ""       // TODO find the version
	return nil
}
